package com.nCinga.dao;

import com.nCinga.bo.Admin;
import com.nCinga.bo.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BookDao {
    private List<Book> books;

    private BookDao() {
        this.books = new ArrayList<Book>();
    }

    private static BookDao bookDao;

    public static BookDao getBookDao() {
        if (bookDao == null) {
            bookDao = new BookDao();
            return bookDao;
        }
        return bookDao;
    }

    public void add(Book book) {
        this.books.add(book);

    }

    public void update(Book book, Book bookNew) {
        int pos = books.indexOf(book);
        books.set(pos, bookNew);


    }

    public void remove(Book book) {
        this.books.remove(book);

    }

    public Book find(int id) {
        for (Book book : books) {
            if (book.getId() == id) {
                return book;
            }

        }
        return null;
    }


}