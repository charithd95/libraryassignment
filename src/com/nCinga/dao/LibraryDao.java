package com.nCinga.dao;

import com.nCinga.bo.Book;
import com.nCinga.bo.Library;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LibraryDao {
    private List<Library> libraries;

    public LibraryDao() {
        this.libraries = new ArrayList<Library>();
    }

    private static LibraryDao libraryDao;

    public static LibraryDao getLibraryDao() {
        if (libraryDao == null) {
            libraryDao = new LibraryDao();
            return libraryDao;
        }
        return libraryDao;
    }

    public void add(Library library) {
        this.libraries.add(library);

    }

    public void update(Library library, Library libraryNew) {
        int pos = libraries.indexOf(library);
        libraries.set(pos, libraryNew);


    }

    public void remove(Library library) {
        this.libraries.remove(library);

    }

    public Library find(int id) {
        for (Library library : libraries) {
            if (library.getLibraryId() == id) {
                return library;
            }

        }
        return null;
    }


}
