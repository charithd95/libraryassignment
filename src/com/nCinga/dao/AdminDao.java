package com.nCinga.dao;

import com.nCinga.bo.Admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdminDao {
    private List<Admin> admins;
    private static  AdminDao adminDao;

    private AdminDao() {
        this.admins = new ArrayList<Admin>();
    }

    public static AdminDao getAdminObject(){
        if (adminDao==null){
            adminDao= new AdminDao();
        }
        return adminDao;
    }

    public void add(Admin admin){
        this.admins.add(admin);

    }
    public void update(Admin admin, Admin adminNew){
        int pos =admins.indexOf(admin);
        admins.set(pos, adminNew);


    }
    public void remove(Admin admin){
        this.admins.remove(admin);

    }
    public Admin find(int id){
        for (Admin admin:admins){
            if (admin.getIdentityNumber()==id){
                return admin;
            }

        }
        return null;

    }


}
