package com.nCinga.dao;

import com.nCinga.bo.Book;
import com.nCinga.bo.Student;
import com.nCinga.bo.enums.Degree;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StudentDao {
    private List<Student> students ;

    public StudentDao() {
        this.students = new ArrayList<Student>();
    }
    public void add(Student student){
        this.students.add(student);
    }
    private static StudentDao studentDao;
    public static StudentDao getStudentDao(){
        if (studentDao==null){
            studentDao=new StudentDao();
            return studentDao;
        }
        return studentDao;
    }
    public void update(Student student, Student studentNew){
        int pos =students.indexOf(student);
        students.set(pos, studentNew);

    }
    public void remove(Student student){
        this.students.remove(student);

    }

    public List<Student> getStudents() {
        return students;
    }
    public Student find(int rollNo) {
        for (Student student : students) {
            if (student.getRollNumber() == rollNo) {
                return student;
            }
            return null;

        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StudentDao)) return false;
        StudentDao that = (StudentDao) o;
        return Objects.equals(getStudents(), that.getStudents());
    }


}
