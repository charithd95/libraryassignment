package com.nCinga.dao;

import com.nCinga.bo.Book;
import com.nCinga.bo.IssuedBookRecord;
import com.nCinga.bo.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IssueBookRecordDao {
    private List<IssuedBookRecord> issuedBookRecords;

    private IssueBookRecordDao() {
        this.issuedBookRecords = new ArrayList<IssuedBookRecord>();
    }
    private static IssueBookRecordDao issueBookRecorddao=new IssueBookRecordDao();
    public static IssueBookRecordDao getIssueBookDao(){
        if (issueBookRecorddao==null){
            issueBookRecorddao=new IssueBookRecordDao();
            return issueBookRecorddao;
        }
        return issueBookRecorddao;

    }

    public List<IssuedBookRecord> getIssuedBookRecords() {
        return issuedBookRecords;
    }

    public void add(IssuedBookRecord issuedBookRecord){
        issuedBookRecords.add(issuedBookRecord);

    }
    public void update(IssuedBookRecord issuedBookRecord, IssuedBookRecord issuedBookRecordNew){
        int pos =issuedBookRecords.indexOf(issuedBookRecord);
        issuedBookRecords.set(pos, issuedBookRecordNew);


    }
    public void remove(IssuedBookRecord issuedBookRecord){
        this.issuedBookRecords.remove(issuedBookRecord);

    }
    public IssuedBookRecord find(int id) {
        for (IssuedBookRecord issuedBookRecord : issuedBookRecords) {
            if (issuedBookRecord.getId() == id) {
                return issuedBookRecord;
            }

        }
        return null;
    }


}
