package com.nCinga.dao;

import com.nCinga.bo.Book;
import com.nCinga.bo.IssuedBookRecord;
import com.nCinga.bo.LibraryBookRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LibraryBookRecordDao {
    private List<LibraryBookRecord> libraryBookRecord ;

    public List<LibraryBookRecord> getLibraryBookRecord() {
        return libraryBookRecord;
    }

    public LibraryBookRecordDao() {
        this.libraryBookRecord = new ArrayList<LibraryBookRecord>();
    }
    private static LibraryBookRecordDao libraryBookRecordDao;
    public static LibraryBookRecordDao getLibraryBookRecordDao(){
        if (libraryBookRecordDao==null){
            libraryBookRecordDao=new LibraryBookRecordDao();
            return libraryBookRecordDao;
        }
        return libraryBookRecordDao;
    }
    public void add(LibraryBookRecord libraryBookRecord){
        this.libraryBookRecord.add(libraryBookRecord);

    }
    public void update(LibraryBookRecord libraryBookRecord, LibraryBookRecord libraryBookRecordNew){
        int pos =this.libraryBookRecord.indexOf(libraryBookRecord);
        this.libraryBookRecord.set(pos, libraryBookRecordNew);


    }
    public void remove(LibraryBookRecord libraryBookRecord){
        this.libraryBookRecord.remove(libraryBookRecord);

    }
    public LibraryBookRecord find(Book book) {
        for (LibraryBookRecord libraryBookRecord : libraryBookRecord) {
            if (libraryBookRecord.getBook() == book) {
                return libraryBookRecord;
            }

        }
        return null;
    }


}
