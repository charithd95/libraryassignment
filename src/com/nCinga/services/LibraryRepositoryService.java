package com.nCinga.services;

import com.nCinga.bo.IssuedBookRecord;
import com.nCinga.bo.Library;
import com.nCinga.bo.Student;
import com.nCinga.dao.IssueBookRecordDao;
import com.nCinga.dao.LibraryBookRecordDao;

import java.awt.print.Book;

public interface LibraryRepositoryService {
    public void issueBook(IssuedBookRecord issuedBookRecord, LibraryBookRecordDao libraryBookRecordDao, IssueBookRecordDao issueBookRecordDao);
}
