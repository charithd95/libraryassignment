package com.nCinga.services;

import com.nCinga.bo.*;
import com.nCinga.bo.enums.Degree;
import com.nCinga.dao.IssueBookRecordDao;
import com.nCinga.dao.LibraryBookRecordDao;
import com.nCinga.dao.LibraryDao;

public class LibraryRepositoryServiceImpl implements LibraryRepositoryService {
    public LibraryRepositoryServiceImpl() {
    }

    public void issueBook(IssuedBookRecord issuedBookRecord, LibraryBookRecordDao libraryBookRecordDao, IssueBookRecordDao issueBookRecordDao){
        Student student = issuedBookRecord.getStudent();
        Admin admin = issuedBookRecord.getAdmin();
        Book book = issuedBookRecord.getBook();
        Library library = issuedBookRecord.getLibrary();

        for (LibraryBookRecord libraryBookRecord:libraryBookRecordDao.getLibraryBookRecord()){
            if (libraryBookRecord.getBook().getId()==book.getId()){

                if (libraryBookRecord.getCountOfBooks()>0){

                int numberOfBooks = 0;
                for (IssuedBookRecord issuedBookRecord1:issueBookRecordDao.getIssuedBookRecords()){
                    if (issuedBookRecord1.getStudent().getRollNumber()==student.getRollNumber()){
                        numberOfBooks++;
                    }
                }

                if (student.getDegree()== Degree.BTECH){
                    if (numberOfBooks<5){
                        //issue book
                    }else {
                        //throw exception
                    }
                }else {
                    if (numberOfBooks<10){
                        //issue book
                    }else {
                        //throw exception
                    }
                }
            }else {
                   //throw exception
                }
            }else {
                //throw exception
            }
        }
    }


}
