package com.nCinga.services;

import com.nCinga.bo.Student;
import com.nCinga.dao.StudentDao;

public interface StudentService {
    public StudentDao addStudent(Student student,StudentDao studentDao);
}
