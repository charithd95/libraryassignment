package com.nCinga.services;

import com.nCinga.bo.Student;
import com.nCinga.dao.StudentDao;

public class StudentServiceImpl implements StudentService {

    @Override
    public StudentDao addStudent(Student student, StudentDao studentDao) {
        studentDao.add(student);
        return studentDao;
    }
}
