package com.nCinga;

import com.nCinga.bo.Student;
import com.nCinga.bo.enums.Degree;
import com.nCinga.dao.StudentDao;
import com.nCinga.services.StudentServiceImpl;

public class Main {

    public static void main(String[] args) {
        StudentDao studentDao = new StudentDao();
        StudentServiceImpl studentService = new StudentServiceImpl();

        Student s1 = new Student("Charith",123,15,"entc", Degree.BTECH,0);
        Student s2 = new Student("rasinda",123,14,"entc", Degree.MTEC,0);
        Student s3 = new Student("rashmin",123,15,"entc", Degree.BTECH,0);
        studentService.addStudent(s1,studentDao);

        System.out.println(studentDao.getStudents().size());

        studentService.addStudent(s2,studentDao);
        System.out.println(studentDao.getStudents().size());
        studentService.addStudent(s3,studentDao);
        System.out.println(studentDao.getStudents().size());

    }
}
