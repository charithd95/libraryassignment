package com.nCinga.bo;

import com.nCinga.bo.enums.Degree;

import java.util.Objects;

public class Student {
    private String name;
    private int rollNumber;
    private int batch;
    private String section;
    private Degree degree;

    public Student(String name, int rollNumber, int batch, String section, Degree degree,int booksCount) {
        setName(name);
        setBatch(batch);
        setDegree(degree);
        setRollNumber(rollNumber);
        setSection(section);
    }

    public void setName(String name) {
        this.name = name;
    }

    private void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

    private void setBatch(int batch) {
        this.batch = batch;
    }

    private void setSection(String section) {
        this.section = section;
    }

    private void setDegree(Degree degree) {
        this.degree = degree;
    }

    public String getName() {
        return name;
    }

    public Degree getDegree() {
        return degree;
    }

    public int getRollNumber() {
        return rollNumber;
    }

    public int getBatch() {
        return batch;
    }

    public String getSection() {
        return section;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getRollNumber() == student.getRollNumber() &&
                getBatch() == student.getBatch() &&
                Objects.equals(getName(), student.getName()) &&
                Objects.equals(getSection(), student.getSection()) &&
                getDegree() == student.getDegree();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getRollNumber(), getBatch(), getSection(), getDegree());
    }
}
