package com.nCinga.bo;

import java.util.Objects;

public class IssuedBookRecord {
    private Admin admin;
    private Student student;
    private Book book;
    private  Library library;
    private static int id;

    public IssuedBookRecord(Admin admin, Student student, Book book, Library library) {
        setAdmin(admin);
        setStudent(student);
        setBook(book);
        setLibrary(library);
        id++;


    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    private void setAdmin(Admin admin) {
        this.admin = admin;
    }

    private void setStudent(Student student) {
        this.student = student;
    }

    private void setLibrary(Library library) {
        this.library = library;
    }

    public Library getLibrary() {
        return library;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Admin getAdmin() {
        return admin;
    }

    public Student getStudent() {
        return student;
    }

    public Book getBook() {
        return book;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssuedBookRecord)) return false;
        IssuedBookRecord that = (IssuedBookRecord) o;
        return getId() == that.getId() &&
                Objects.equals(getAdmin(), that.getAdmin()) &&
                Objects.equals(getStudent(), that.getStudent()) &&
                Objects.equals(getBook(), that.getBook()) &&
                Objects.equals(getLibrary(), that.getLibrary());
    }

}
