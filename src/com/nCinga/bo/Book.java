package com.nCinga.bo;

import com.nCinga.bo.enums.Subject;

import java.util.Objects;

public class Book {
    private int id;
    private String name;
    private Subject subject;

    public Book(String name, Subject subject) {
        setName(name);
        setSubject(subject);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public Subject getSubject() {
        return subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getId() == book.getId() &&
                Objects.equals(getName(), book.getName()) &&
                getSubject() == book.getSubject();
    }

}
