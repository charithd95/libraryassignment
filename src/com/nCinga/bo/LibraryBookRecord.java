package com.nCinga.bo;

import java.util.Objects;

public class LibraryBookRecord {
    private Library library;
    private Book book;
    private int countOfBooks;

    public LibraryBookRecord(Library library, Book book, int countOfBooks) {
        this.library = library;
        setLibrary(library);
        setBook(book);
        setCountOfBooks(countOfBooks);
    }

    private void setLibrary(Library library) {
        this.library = library;
    }

    private void setBook(Book book) {
        this.book = book;
    }

    public void setCountOfBooks(int countOfBooks) {
        this.countOfBooks = countOfBooks;
    }

    public Library getLibrary() {
        return library;
    }

    public Book getBook() {
        return book;
    }

    public int getCountOfBooks() {
        return countOfBooks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LibraryBookRecord)) return false;
        LibraryBookRecord that = (LibraryBookRecord) o;
        return getCountOfBooks() == that.getCountOfBooks() &&
                Objects.equals(getLibrary(), that.getLibrary()) &&
                Objects.equals(getBook(), that.getBook());
    }


}
