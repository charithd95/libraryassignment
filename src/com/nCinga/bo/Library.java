package com.nCinga.bo;

import java.util.Objects;

public class Library {
    private String libraryName;
    private int libraryId;

    public Library(String libraryName, int libraryId) {
        setLibraryName(libraryName);
        setLibraryId(libraryId);
    }
    private void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }
    private void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }
    public String getLibraryName() {
        return libraryName;
    }
    public int getLibraryId() {
        return libraryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Library)) return false;
        Library library = (Library) o;
        return getLibraryId() == library.getLibraryId() &&
                Objects.equals(getLibraryName(), library.getLibraryName());
    }


}
