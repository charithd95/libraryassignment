package com.nCinga.bo;

import java.util.Objects;

public class Admin {
    private String name;
    private int identityNumber;

    public Admin(String name, int identityNumber) {
        setName(name);
        setIdentityNumber(identityNumber);
    }
    private void setName(String name) {
        this.name = name;
    }

    private void setIdentityNumber(int identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getName() {
        return name;
    }

    public int getIdentityNumber() {
        return identityNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Admin)) return false;
        Admin admin = (Admin) o;
        return getIdentityNumber() == admin.getIdentityNumber() &&
                Objects.equals(getName(), admin.getName());
    }

}

